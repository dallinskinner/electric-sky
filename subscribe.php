<?php

$list_id = 'ce02ade10a';
$url = "https://us19.api.mailchimp.com/3.0/lists/$list_id";

$key = '2065c0aea8b971f3beab2f5045e707b0-us19';
$username = 'whatevz';

$post = json_decode(file_get_contents('php://input'), true);

$data = array(
  'update_existing' => true,
  'members' => array(
    array(
      'email_address' => $post['email'],
      'status' => 'subscribed',
    )
  ),
);

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $key);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

$output = curl_exec($ch);
$response = json_decode($output, true);

curl_close($ch);

echo json_encode(array(
  'total_updated' => $response['total_updated'],
  'total_created' => $response['total_created'],
  'error_count' => $response['error_count']
));
