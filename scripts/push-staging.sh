#!/bin/bash

cd /Users/me/projects/electric-sky
npm run build

scp /Users/me/projects/electric-sky/index.html serverpilot@cursor.studio:~/apps/es-dev/public
scp /Users/me/projects/electric-sky/subscribe.php serverpilot@cursor.studio:~/apps/es-dev/public
rsync -avzhe ssh /Users/me/projects/electric-sky/dist serverpilot@cursor.studio:~/apps/es-dev/public