import device from 'current-device';
import iNoBounce from 'inobounce';
import axios from 'axios';
import '../../assets/videos/bubbly-web.mp4';
import '../../assets/videos/noir-web.mp4';
import '../../assets/videos/pinot-web.mp4';
import '../../assets/videos/rose-web.mp4';
import '../../assets/img/cut-out.svg';
import '../../assets/img/logo.svg';
import '../../assets/img/bubbly-still.png';
import maskImageUrl from '../../assets/img/can-mask.png';
import '../../scss/base.scss';
import '../../scss/typography.scss';
import '../../scss/gate.scss';
import '../../scss/spin.scss';

(function gate() {
  document.body.addEventListener('touchmove', (e) => {
    e.preventDefault();
  });

  iNoBounce.enable();

  const flavors = ['bubbly', 'noir', 'pinot', 'rose'];
  const yesButton = document.querySelector('#js-yes');
  const container = document.querySelector('#js-container');
  const form = document.querySelector('#js-subscribe-form');
  const input = document.querySelector('#js-subscribe-input');
  const button = document.querySelector('#js-subscribe-button');

  form.addEventListener('submit', (e) => {
    e.preventDefault();

    button.innerText = 'Sending...';
    button.disabled = true;

    axios
      .post('/subscribe.php', {
        email: input.value,
      })
      .then((result) => {
        if (result.data.error_count === 0) {
          button.innerText = 'Subscribe';
          input.value = '';
          input.placeholder = 'Thank You';

          setTimeout(() => {
            input.placeholder = 'Your Email Address';
            button.disabled = false;
          }, 3000);
        } else {
          const email = input.value;
          input.value = '';
          input.placeholder = 'Error. Try again Later';

          setTimeout(() => {
            input.value = email;
            input.placeholder = 'Your Email Address';
            button.innerText = 'Subscribe';
            button.disabled = false;
          }, 3000);
        }
      });
  });

  const cans = {};

  flavors.forEach((flavor) => {
    cans[flavor] = {
      video: document.querySelector(`#js-${flavor}-can`),
      loaded: false,
    };
  });

  if (device.mobile()) {
    flavors.forEach((key) => {
      cans[key].video.play();
    });
  } else {
    flavors.forEach((flavor) => {
      cans[flavor].video.addEventListener('canplaythrough', () => {
        cans[flavor].loaded = true;

        let shouldStart = true;

        flavors.forEach((subflavor) => {
          if (!cans[subflavor].loaded) {
            shouldStart = false;
          }
        });

        if (shouldStart) {
          flavors.forEach((key) => {
            cans[key].video.play();
          });
        }
      });
    });
  }

  const canvas = document.querySelector('#js-canvas');
  const ctx = canvas.getContext('2d');
  const maskImage = new Image();

  let maskLoaded = false;
  let currentFlavorIndex = 0;
  let lastX = 0;
  let lastY = 0;
  let lastMobileX = 0;
  let lastMobileY = 0;
  let ticking = false;

  function updateCanvas() {
    const currentCan = cans[flavors[currentFlavorIndex]].video;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.drawImage(currentCan, 0, 0, 1440, 1440);
    if (maskLoaded) {
      ctx.globalCompositeOperation = 'destination-out';
      ctx.drawImage(maskImage, 0, 0, 1440, 1440);
      ctx.globalCompositeOperation = 'source-over';
    }

    requestAnimationFrame(updateCanvas);
  }

  maskImage.onload = () => {
    maskLoaded = true;
    updateCanvas();
  };
  maskImage.src = maskImageUrl;

  function getLineLength(x1, y1, x2, y2) {
    const a = (x2 - x1) ** 2;
    const b = (y2 - y1) ** 2;

    return parseInt(Math.sqrt(a + b), 10);
  }

  function getNextFlavor() {
    let nextIndex = currentFlavorIndex + 1;

    if (nextIndex >= flavors.length) {
      nextIndex = 0;
    }

    return flavors[nextIndex];
  }

  function changeColor() {
    const oldFlavor = flavors[currentFlavorIndex];
    const nextFlavor = getNextFlavor();

    document.body.classList.remove(oldFlavor);
    document.body.classList.add(nextFlavor);

    currentFlavorIndex = flavors.indexOf(nextFlavor);
  }

  function switchColor(e) {
    const threshold = 50;

    const currentX = e.pageX;
    const currentY = e.pageY;

    if (getLineLength(lastX, lastY, currentX, currentY) > threshold) {
      lastX = currentX;
      lastY = currentY;

      changeColor();
    }
  }

  function switchColorMobile(e) {
    const threshold = 10;

    const currentX = e.beta;
    const currentY = e.gamma;

    if (
      Math.abs(lastMobileX - currentX) > threshold
      || Math.abs(lastMobileY - currentY > threshold)
    ) {
      lastMobileX = currentX;
      lastMobileY = currentY;

      changeColor();
    }
  }

  function initSpinner() {
    if (window.DeviceOrientationEvent && 'ontouchstart' in window) {
      window.addEventListener('deviceorientation', switchColorMobile);
    } else {
      document.body.addEventListener('mousemove', (e) => {
        if (!ticking) {
          window.requestAnimationFrame(() => {
            switchColor(e);
            ticking = false;
          });

          ticking = true;
        }
      });
    }
  }

  yesButton.addEventListener('click', (e) => {
    e.preventDefault();
    container.classList.add('show-can');
    initSpinner();
  });
}());
